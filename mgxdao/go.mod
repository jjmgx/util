module mgxdao

go 1.13

require (
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/go-sql-driver/mysql v1.4.1
)
